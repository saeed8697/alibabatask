import { useState, useEffect } from "react";
import "../styles/globals.css";
import "../styles/tailwind.css";
import 'nprogress/nprogress.css';
import nProgress from 'nprogress'
import * as ga from '../lib/ga'
import { useRouter } from 'next/router'
import {AppWrapper} from "../context/filter.context";


function MyApp({ Component, pageProps }) {
	const [isMounted, setIsMounted] = useState(false);
	const router = useRouter()

	useEffect(() => {
		setIsMounted(true);
		const handleRouteChange = (url) => {
			nProgress.done

			ga.pageview(url)
		}
		//When the component is mounted, subscribe to router changes
		//and log those page views
		router.events.on('routeChangeComplete', handleRouteChange)
		router.events.on('routeChangeComplete', nProgress.done)
		router.events.on('routeChangeStart', nProgress.start)
		router.events.on('routeChangeError', nProgress.done)
		// If the component is unmounted, unsubscribe
		// from the event with the `off` method
		return () => {
			router.events.off('routeChangeComplete', handleRouteChange)
		}
	}, [router.events])

	return (
		isMounted && (
			<AppWrapper>
				<Component {...pageProps} />
			</AppWrapper>
		)
	);
}

export default MyApp;

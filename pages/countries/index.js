import {useState} from "react";
import Link from "next/link";
import Loading from "../../components/Loading";
import Navbar from "../../components/Navbar";
import SearchBar from "../../components/SearchBar";
import RegionFilter from "../../components/RegionFilter";
import dynamic from "next/dynamic";
import SortFilter from "../../components/SortFilter";
import * as ga from "../../lib/ga";


/**
 *
 * main page of country
 * @param countries
 * @returns {JSX.Element}
 * @constructor
 */

const DynamicComponentCardCountry = dynamic(() => import('../../components/CountryCard'),
    {loading: () => <p>data is loading (Lazy)</p>})
export default function Main({countries}) {
    const [search, setSearch] = useState("");
    const [filter, setFilter] = useState("");
    const [sortfilter, setSortFilter] = useState({
        name: "Country Name - ASC ",
        prop: "name",
        sorttype: true
    });



    return (
        <>
            <Navbar/>
            <main className="max-w-screen-xl min-h-screen pb-16 mx-8 md:mx-16 xl:mx-auto">
                <div className="flex-row items-center justify-between py-8 sm:flex sm:mb-16">
                    <SearchBar search={search} setSearch={setSearch}/>
                    <RegionFilter filter={filter} setFilter={setFilter}/>
                    <SortFilter sortfilter={sortfilter} setSortFilter={setSortFilter}/>
                </div>

                <div className="grid grid-cols-1 gap-12 md:grid-cols-2 xl:grid-cols-4">
                    {countries &&
                    countries
                        .filter(
                            (country) =>
                                country.region.includes(filter) &&
                                country.name.toLowerCase().includes(search)
                        ).sort((a, b) => {
                        return (a[sortfilter.key] < b[sortfilter.key] ? -1 : 1) * (sortfilter.sorttype ? 1 : -1)
                    })

                        .map((country) => (
                            <Link


                                href={`/countries/${country.alpha3Code}`}
                                key={country.alpha3Code}

                            >
                                <div>
                                    <DynamicComponentCardCountry country={country}/>
                                </div>
                            </Link>
                        ))}
                </div>
            </main>
        </>
    );
}

export async function getStaticProps() {
    const res = await fetch("https://restcountries.com/v2/all");
    const data = await res.json();
    debugger

    if (!data) {
        return {
            notFound: true
        };
    }
    return {
        props: {countries: data},
    };
}

# Saeed Hasanabadi - Front end position  - Next js with Tailwindcss 
You can find demo here [Link to demo](https://alibabatask.herokuapp.com/countries) 


## Countries task 

Thanks for checking out this front-end coding challenge.

## The Task 

integrate with the [REST Countries API](https://restcountries.com) to pull country data and display it .


Project contains : 

- See all countries from the API on the homepage
- Search for a country using an `input` field
- Filter countries by region
- Click on a country to see more detailed information on a separate page
- Click through to the border countries on the detail page
- Toggle the color scheme between light and dark mode 
- Sorting based on country name and population 
- collect analytical data using Google Analytics





## Usage

### Installation

From root folder of project :

```sh
npm install
```

### Development

To run on development :

```bash
npm run dev
# or
yarn dev
```

### Production

To compile for production build:

```sh
npm run build
# or
yarn build
```

## Features 
- Next js for handling ssr and routing 
- Tailwindcss 
- dynamic import for lazy loading 
- dockerize project








import Image from 'next/image'

/****
 * component for handling and styling main country card
 * @param country
 * @returns {JSX.Element}
 * @constructor
 */
const CountryCard = ({ country }) => {
	return (
		<div>
			<div className="card">
				<img
					className="object-cover w-full h-56 md:h-48 lg:h-72 xl:h-48"
					src={country.flag}
					alt={`The flag of ${country.name}`}
				/>

				<div className="p-8 leading-relaxed">
					<h2 className="mb-4 text-lg font-bold">{country.name}</h2>
					<div>
						<span className="font-semibold">Population: </span>
						{country.population.toLocaleString()}
					</div>
					<div>
						<span className="font-semibold">Region: </span>
						{country.region}
					</div>
					<div>
						<span className="font-semibold">Capital: </span>
						{country.capital}
					</div>
				</div>
			</div>
		</div>
	);
};

export default CountryCard;

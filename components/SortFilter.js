import {useState} from "react";
import {GoChevronDown} from "react-icons/go";

/**
 *
 *
 * hanfling sorttng based on Country Name & population
 * @param sortfilter
 * @param setSortFilter
 * @returns {JSX.Element}
 * @constructor
 */
const SortFilter = ({sortfilter, setSortFilter}) => {
    const [toggleMenu, setToggleMenu] = useState(false);

    const regions = [{
        name: "Country Name -ASC ",
        key: "name",
        sorttype: true
    },
        {
            name: "Country Name - DESC ",
            key: "name",
            sorttype: false
        },
        {
            name: "population - ASC ",
            key: "population",
            sorttype: true
        },
        {
            name: "population - DESC ",
            key: "population",
            sorttype: false
        }];

    return (
        <div className="relative sm:w-64">
            <div
                onClick={() => setToggleMenu(!toggleMenu)}
                onBlur={() => setToggleMenu(false)}
                className="overflow-hidden rounded-md shadow cursor-pointer bg-light-elements dark:bg-dark-elements dark:text-light-elements focus-within:ring"
            >

                <form className="flex items-center justify-between w-full p-4 focus:outline-none hover:opacity-75">
                    {sortfilter ? sortfilter.name : "Country Name -ASC "}
                    <GoChevronDown
                        className={`transform transition-transform ml-4 ${toggleMenu && "rotate-180"
                        }`}
                    />
                </form>
            </div>
            {toggleMenu && (
                <div
                    className="absolute z-10 flex flex-col w-full mt-2 overflow-hidden rounded-md shadow text-dark-elements bg-light-elements dark:bg-dark-elements dark:text-light-elements focus:outline-none"
                    onClick={() => setToggleMenu(false)}
                >


                    {regions.map((region) => (
                        <button
                            key={region.key}
                            onMouseDown={(e) => {
                                e.preventDefault();
                            }}
                            onClick={() => setSortFilter(region)}
                            className="p-2 text-left focus:outline-none dark:hover:bg-gray-700 hover:bg-gray-100"
                        >
                            {region.name}
                        </button>
                    ))}
                </div>
            )
            }
        </div>
    );
};

export default SortFilter;
